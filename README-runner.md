Gitlab Runner einrichten
========================

Der GitLab Runner kann mittels eines HELM Charts eingerichtet werden.

**Installation GitLab Runner** (falls nicht bereits erfolgt)

Dazu in microk8s zuerst HELM3 aktivieren. GitLab Repo hinzufügen und GitLab Runner installieren.

    microk8s enable helm3
    microk8s helm3 repo add gitlab https://charts.gitlab.io
    microk8s kubectl create namespace gitlab
    microk8s helm3 install -n gitlab gitlab-runner gitlab/gitlab-runner  

Die Original Anleitung findet man [hier](https://docs.gitlab.com/runner/install/kubernetes.html).

**GitLab Runner Registrieren**

Nach der Installation ist der GitLab Runner zu registieren.

Den entsprechenden Token findet man im GitLab Repository, des Projektes, unter -> Settings -> CD/CD -> Runners

    export REGISTRATION_TOKEN=<Ctrl+p>
    microk8s helm3 upgrade -n gitlab gitlab-runner --set gitlabUrl=https://gitlab.com,runnerRegistrationToken=${REGISTRATION_TOKEN} gitlab/gitlab-runner

Im GitLab UI ist für den Runner der Tag `kubernetes` zu setzen. Damit für den CI/CD Job der registrierte Runner verwendet wird.

    
