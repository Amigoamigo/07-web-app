Continous Deployment - pull
===========================

Auf der Kubernetes Zielplattform muss GitLab Runner installiert sein.

Das aktuelle Projekt, muss mit diesem GitLab Runner verbunden sein, bzw. diesen Ansteuern können.

Bei der Konfigurations können die Default Werte übernommen werden, ausser:
* executor: kubernetes
* tags: kubernetes

Anschliessend könnt Ihr `.gitlab-ci.yaml` wie folgt Erweitern bzw. Ändern:

    image: maven:3-alpine
    
    build_job:
        stage: build
        before_script:
          - apk update
          - apk add gettext
        script:
            - mvn clean compile assembly:single
            - 'envsubst <deployment.yaml >target/deployment.yaml'
            - 'envsubst <service.yaml >target/service.yaml'
        artifacts:
            paths:
                - target


    # Deploy to Kubernetes
    deploy_job:
      stage: deploy
      tags:
        - kubernetes
      # Container Image mit kubectl
      image: bitnami/kubectl:latest
      script:
        - kubectl apply -n default -f target/deployment.yaml
        - kubectl apply -n default -f target/service.yaml  
    
      needs:
        - job: release_job
          artifacts: true  
      rules:
      - if: "$CI_COMMIT_TAG"
      


